#!/usr/bin/env bash

set -euxo pipefail

PIP_TOOLS="tox ansible flake8 black isort"

echo "Yum update"
sudo yum update -y 

echo "Make local bin dir"
mkdir -p $HOME/.local/bin

echo "Install tools"
sudo yum install -y yum-utils
sudo yum-config-manager --add-repo https://rpm.releases.hashicorp.com/AmazonLinux/hashicorp.repo
sudo amazon-linux-extras enable python3.8
sudo yum -y install python3.8 terraform git tmux jq

for tool in $PIP_TOOLS
do
    echo "Install and setup $tool"
    python3.8 -m venv $HOME/.${tool}base
    $HOME/.${tool}base/bin/pip install --upgrade pip
    $HOME/.${tool}base/bin/pip install $tool
    if [ ! -f $HOME/.local/bin/$tool ]
    then
        ln -s $HOME/.${tool}base/bin/$tool $HOME/.local/bin/$tool
    fi
    $tool --version
done
